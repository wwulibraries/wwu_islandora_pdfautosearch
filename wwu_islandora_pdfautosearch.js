// https://bitbucket.org/wwulibraries/wwu_islandora_pdfautosearch/src/master/

jQuery(window).load(function(){

    if (document.getElementById('islandora-pdfjs')) {
        // var theFrame = document.getElementById('islandora-pdfjs').getElementsByTagName('iframe')[0];
    
        function checkStatus() {
            // console.log('checking status');
            var pdfStatus = document.getElementById('islandora-pdfjs').getElementsByTagName('iframe')[0].contentWindow.PDFViewerApplication.downloadComplete;
            // console.log('pdfStatus', pdfStatus);
            if (pdfStatus) {
                runSearch();
                clearInterval(checkStatusInterval);
            }
        }
    
        var checkStatusInterval = setInterval ( checkStatus, 500 );     //  check the status every 1/2 second to see if the PDF has loaded
    
        // theFrame.addEventListener("pagesloaded", runSearch, false);
        
        function runSearch() {
            if (location.search) {
                // use the querystring to get the search query if it exists
                var urlParams = new URLSearchParams(window.location.search);
                var searchForParameter = urlParams.get('search');
                if (searchForParameter) {
                    var searchFor = searchForParameter;
                }
            }

            if (!searchFor) {
                // if searchFor was not defined above, let's try to find the query in the prev/next button code
                if (document.getElementById('islandora-solr-search-return-link')) {
                    // look for the search query in the 'search navigation block'
                    var searchUrl = document.getElementById('islandora-solr-search-return-link').getElementsByTagName('a')[0].pathname;     // "/islandora/search/%22peace%20corps%22"
                    var searchFor = searchUrl.split('/').pop();         // get the last part of the array
                    var searchFor = decodeURIComponent(searchFor).trim();       // decode URL and remove leading or trailing spaces
                    var searchFor = searchFor.replace(/^\"+|\"+$/g, '');        // remove leading and trailing quotes; hat tip to https://stackoverflow.com/a/32516190
                }
            }

            // thanks to  https://github.com/mozilla/pdf.js/issues/1875

            if (searchFor) {
                console.log('searching for ' + searchFor);
                document.getElementsByClassName("pdf")[0].src += '#search="' + searchFor + '"';

                var pdfWindow = document.getElementsByClassName("pdf")[0].contentWindow;
                pdfWindow.PDFViewerApplication.findBar.open(); 
                pdfWindow.PDFViewerApplication.findBar.findField.value = searchFor; 
                pdfWindow.PDFViewerApplication.findBar.highlightAll.checked= true; 
                pdfWindow.PDFViewerApplication.findBar.findNextButton.click(); 
            
                // var event = new Event('click');
                // document.getElementsByClassName("pdf")[0].contentDocument.getElementById('viewFind').dispatchEvent(event);            
                // document.getElementsByClassName("pdf")[0].contentDocument.getElementById('findHighlightAll').checked = true;
                // document.getElementsByClassName("pdf")[0].contentDocument.getElementById('findInput').value = searchFor;
                // var event = new Event('input');
                // document.getElementsByClassName("pdf")[0].contentDocument.getElementById('findInput').dispatchEvent(event);
        
            }
        }
    }
    
})
